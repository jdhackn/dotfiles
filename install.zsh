# Adds a bootstrapping for our dotfiles zsh configs
echo "Adding initialization for Zsh dotfiles to the .zshrc"
echo "\n\n# Bootstrap the DOTDIR zsh init file" >> $HOME/.zshrc
echo '. $HOME/.dotfiles/init.zsh' >> $HOME/.zshrc

# Symlinks to configs
echo "Symlinking to dotfiles configs"
export DOTDIR=$HOME/.dotfiles
ln -sf $DOTDIR/tmux/tmux.conf $HOME/.tmux.conf
